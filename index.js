const isurl = require('is-url');
const path = require('path');
const fs = require('fs');
const mustache = require('mustache');
const debug = require('debug')(require('./package.json').name)

exports.command = 'notify <hook> <notification>'
exports.desc = 'Send a JSON payload to the webhook'
exports.builder = (yargs) => yargs
  .positional('hook', {
    describe: 'Webhook link',
    type: 'string'
  })
  .positional('notification', {
    describe: `Webhook JSON mustache template, that will be processed with provided payload`,
    type: 'string'
  })
  .check(args => {
    if (!isurl(args.hook))
      throw new Error('Provided hook does not look like url');
    if (!fs.existsSync(args.notification))
      throw new Error('Provided notification configuration file does not exist');
    if (path.extname(args.notification) !== '.json')
      throw new Error('Provided notification configuration must have `.json` extension');
    return true;
  })
  .option('payload')
  .describe('payload', 'Pass here any payload to the notification as properties')
  .requiresArg('payload');

exports.handler = ({ payload, notification, hook }) => {
  try {
    const parsed = path.parse(notification);
    const handlerName = path.resolve(__dirname, `request.${parsed.name.split('.').pop()}`);
    const handler = fs.existsSync(handlerName) ? require(handlerName) : require('./request');
    const configuration = JSON.stringify(require(path.resolve(notification)), null, 0);
    const body = JSON.parse(mustache.render(configuration, payload || {}));
    debug('Webhook content:\n%s\n', JSON.stringify(body, null, 1));

    handler(hook, body, (err, response) => err ? console.error(err) : response && debug('Response is: %s\n', JSON.stringify(response, null, 1)));
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}
