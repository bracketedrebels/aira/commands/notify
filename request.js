const https = require('https');
const parser = require('url');
const request = require('request');


module.exports = function (url, body, cb) {
  request({
    url,
    body,
    json: true,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      "Content-Length": body.length
    },
    agent: new https.Agent({
      host: parser.parse(url).hostname,
      port: '443',
      path: '/',
      rejectUnauthorized: false
    })
  }, cb)
}