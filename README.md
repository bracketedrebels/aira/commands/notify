Notify - AIRA module
======================

Allows you to send message to different webhook. Message can be configured in separated JSON file, that can use Mustache template interpolations within. You can place this configuration together with other configuration files in you repository and then use it in CI/CD.

Please, note, that you should name your notiffication configuration file as `*.<appname>.json` in order to module automatically detect, whether should be some custom processing applied to the hook payload or not. If no appname specified processed template will be sent as data directly.

In order to use it you must install [AIRA CLI](https://www.npmjs.com/package/@bracketedrebels/aira), and that plugin. Plugin existance will be detected automatically and `aira notify --help` will show you how to use the plugin.

Example
-------

.notification.slack.json
```json
{
  "color": "good",
  "pretext": "New version {{{v}}} release! :tada:",
  "text": "My awesome library is better now! See changelog: {{{link}}}",
  "title": "Version {{{v}}}",
  "title_link": "{{{link}}}"
}
```

CMD
```shell
  aira notify <webhook> .notification.slack.json --payload.link=<link> --payload.v=<tag>
```
